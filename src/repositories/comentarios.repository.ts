import {DefaultCrudRepository} from '@loopback/repository';
import {Comentarios, ComentariosRelations} from '../models';
import {DbDataSource} from '../datasources';
import {inject} from '@loopback/core';

export class ComentariosRepository extends DefaultCrudRepository<
  Comentarios,
  typeof Comentarios.prototype.id,
  ComentariosRelations
> {
  constructor(
    @inject('datasources.db') dataSource: DbDataSource,
  ) {
    super(Comentarios, dataSource);
  }
}
